﻿Shader "UCG/SliceY"
{
	Properties {
	     _myColour ("Example Colour", Color) = (1,1,1,1)
	     
		 _slice_y("Slice Value",float) = 10
	}
	
	SubShader {
		
		CGPROGRAM
			#pragma surface surf Lambert

			struct Input {
				float2 uvMainTex;
				float3 worldPos;
			};

			fixed4 _myColour;
			float _slice_y;
		
			void surf (Input IN, inout SurfaceOutput o){

				if (IN.worldPos.y<_slice_y){
					o.Albedo = _myColour.rgb;
					
				}else{
					o.Albedo = fixed3 (0,0,0);
				}
			    

			
			}
		
		ENDCG
	}
	
	FallBack "Diffuse"
}
