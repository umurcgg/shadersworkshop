﻿Shader "UCG/Variables"
{
	Properties {
		_mainColor ("MainColor", Color) = (1,1,1,1)
		_mainText ("MainTexture", 2D)= "white"{} 
		_colorMultiplier ("Color Multiplier", Range(0,10)) = 0.5 
		_emissionCube ("Emission Cube", CUBE) =""{}
	     
		
	}
	
	SubShader {
		
		CGPROGRAM
			#pragma surface surf Lambert

			
			fixed4 _mainColor;
			sampler2D _mainText;
			samplerCUBE _emissionCube;
			half _colorMultiplier;
			
		
			struct Input {
				float2 uv_mainText;
				float3 worldRefl;
			};

		

			void surf (Input IN, inout SurfaceOutput o){

				o.Albedo= tex2D(_mainText,IN.uv_mainText).rgb;
				o.Albedo *= _mainColor.rbg;
				o.Albedo *= _colorMultiplier;
				o.Emission = texCUBE(_emissionCube, IN.worldRefl).rgb;
		
			}
		
		ENDCG
	}
	
	FallBack "Diffuse"
}
