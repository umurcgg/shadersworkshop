﻿ Shader "UCG/BumpedEnv"
{
	Properties {
		_mainText ("MainTexture", 2D)= "white"{}
		_normalMap ("NormalTexture", 2D)= "white"{} 
		_normalMultiplierAmount ("Normal Multiplier" , Range(0,10))= 1
		_bumpAmount ("Bump Amount" , Range(0,10))= 1
		_cubeMap ("Cube Map", CUBE) = ""{}
	}
	
	SubShader {
		
		CGPROGRAM
			#pragma surface surf Lambert

			
			sampler2D _normalMap;
			sampler2D _mainText;
			float _bumpAmount;
			float _textureScale;
			float _normalMultiplierAmount;
			samplerCUBE _cubeMap;
		
			struct Input {
				float2 uv_mainText;
				float2 uv_normalMap;
				float3 worldRefl; INTERNAL_DATA
			};

		
			void surf (Input IN, inout SurfaceOutput o){
				o.Albedo= tex2D(_mainText,IN.uv_mainText/_textureScale).rgb;
				o.Normal = UnpackNormal(tex2D(_normalMap, IN.uv_normalMap)) * _normalMultiplierAmount;
				o.Normal*= float3(_bumpAmount,_bumpAmount,1);
				
				o.Emission = texCUBE(_cubeMap,WorldReflectionVector(IN,o.Normal)).rgb;
					
			}
		
		ENDCG
	}
	
	FallBack "Diffuse"
}
