﻿ Shader "UCG/BumpDiffuse"
{
	Properties {
		_mainText ("MainTexture", 2D)= "white"{}
		_normalMap ("NormalTexture", 2D)= "white"{} 
		_bumpAmount ("Bump Amount" , Range(0,10))= 1
		_textureScale("Texture Scale" , Range(0.01,10))= 1
	}
	
	SubShader {
		
		CGPROGRAM
			#pragma surface surf Lambert

			
			sampler2D _normalMap;
			sampler2D _mainText;
			float _bumpAmount;
			float _textureScale;
		
			struct Input {
				float2 uv_mainText;
				float2 uv_normalMap;
				float3 worldRefl;
			};

		
			void surf (Input IN, inout SurfaceOutput o){
				o.Albedo= tex2D(_mainText,IN.uv_mainText/_textureScale).rgb;
				o.Normal = UnpackNormal(tex2D(_normalMap, IN.uv_normalMap/_textureScale));
				o.Normal*= float3(_bumpAmount,_bumpAmount,1);
			}
		
		ENDCG
	}
	
	FallBack "Diffuse"
}
