﻿ Shader "UCG/ZBuffer"
{
	Properties {
	    _mainColor("MainColor",Color)=(0,0,0,0)

		
	}
	
	SubShader {
	    Tags {"Queue"="Geometry+100"}
		ZWrite Off
		
		CGPROGRAM
			#pragma surface surf Lambert
			
	
		    float4 _mainColor;
		
			struct Input {
				float2 uv_mainText;
				float2 uv_normalMap;
				float3 worldRefl;
			};

		
			void surf (Input IN, inout SurfaceOutput o){
				o.Albedo= _mainColor;
	
			}
		
		ENDCG
	}
	
	FallBack "Diffuse"
}
