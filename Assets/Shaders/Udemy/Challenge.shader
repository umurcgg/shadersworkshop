﻿Shader "UCG/Challenge"
{
	Properties {
		
		_mainText ("MainTexture", 2D)= "white"{} 
		
		
	     
		
	}
	
	SubShader {
		
		CGPROGRAM
			#pragma surface surf Lambert

			
		
			sampler2D _mainText;
			
		
			
		
			struct Input {
				float2 uv_mainText;
		
			};

		
			void surf (Input IN, inout SurfaceOutput o){
				fixed3 outputColor=tex2D(_mainText,IN.uv_mainText).rgb;
				outputColor.g=1;
				o.Albedo= outputColor;
		
		


			}
		
		ENDCG
	}
	
	FallBack "Diffuse"
}
