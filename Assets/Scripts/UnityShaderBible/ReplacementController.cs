using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ReplacementController : MonoBehaviour
{
    public Shader replacementShader;
    
    // Start is called before the first frame update
    void OnEnable()
    {
        if (replacementShader)
        {
            GetComponent<Camera>().SetReplacementShader(replacementShader, "RenderType");
        }
    }

    // Update is called once per frame
    void OnDisable()
    {
        GetComponent<Camera>().ResetReplacementShader();
    }
}
